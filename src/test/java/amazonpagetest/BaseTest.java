package amazonpagetest;

import com.google.gson.Gson;
import com.testvagrant.optimuscloud.entities.MobileDriverDetails;
import com.testvagrant.optimuscloud.entities.SessionInfo;
import com.testvagrant.optimuscloud.remote.OptimusCloudDriver;
import com.testvagrant.optimuscloud.remote.OptimusCloudManager;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.net.MalformedURLException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class BaseTest {


    private static SessionInfo sessionInfo;
    protected static MobileDriverDetails mobileDriverDetails;
    protected static AppiumDriver appiumDriver;
    WebDriverWait wait;

    @BeforeSuite
    public void suiteSetup() {
        int sessions = Integer.parseInt(System.getProperty("sessions","1"));
        sessionInfo = new OptimusCloudManager().reserveAndroidSession(sessions);
        System.out.println(sessionInfo.getBuildNo());
    }

    @BeforeMethod
    public void createDriver() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "2a2d916");
        capabilities.setCapability("platformName", "Android");
//        capabilities.setCapability("platfo version", "6.1");
//        capabilities.setCapability("automationName", "appium");
//        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("newCommandTimeout",120);
        capabilities.setCapability("app", "https://github.com/KrishnB/AppRepo/releases/download/v3/amazonx86.apk.zip");
        capabilities.setCapability("appPackage", "in.amazon.mShop.android.shopping");
        capabilities.setCapability("appActivity", "com.amazon.mShop.home.HomeActivity");
        mobileDriverDetails = new OptimusCloudDriver().createDriver(sessionInfo.getBuildNo(), capabilities);
        System.out.println(mobileDriverDetails.getSessionUrl());
        appiumDriver = (AppiumDriver) mobileDriverDetails.getMobileDriver();
        appiumDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        wait = new WebDriverWait(appiumDriver, 10);
    }

    /**
     * Refrain from calling driver.quit() or driver.close() in the tear down as it will affect other tests
     */
    @AfterMethod(alwaysRun = true)
    public void closeDriver() {
        try {
            new OptimusCloudManager().releaseSession(mobileDriverDetails);
            System.out.println("Released session "+mobileDriverDetails.getSessionUrl());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @AfterSuite(alwaysRun = true)
    public void suiteTeardown() {
        try {
            new OptimusCloudManager().unReserveSession(sessionInfo.getBuildNo());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
