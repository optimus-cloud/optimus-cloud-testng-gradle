package amazonpagetest;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;


public class AmazonTestOnCloud extends BaseTest {

    @Test(description = "Check the amzon home icon present on amzon sign in page")
    public void checkAmazonIcone() {
        System.out.println("Test checkAmazonIcone");
        System.out.println("Created mobileDriverDetails --> "+ mobileDriverDetails.getSessionUrl());
        boolean amazonIcon = appiumDriver.findElement(By.id("sso_splash_logo")).isDisplayed();
        Assert.assertTrue(amazonIcon);

    }

    @Test(description = "Check the Amazon sign in page primary title")
    public void amazonSigninPagePrimaryTitle() {
        System.out.println("Test amazonSigninPagePrimaryTitle");
        System.out.println("Created mobileDriverDetails --> "+ mobileDriverDetails.getSessionUrl());
        String pageText1 = appiumDriver.findElement(By.id("signin_to_yourAccount")).getText();
        Assert.assertEquals(pageText1, "Sign in to your account");

        String pageText2 = appiumDriver.findElement(By.id("view_your_wish_list")).getText();
        Assert.assertEquals(pageText2, "View your wish list");

        String pageText3 = appiumDriver.findElement(By.id("Find_purchase")).getText();
        Assert.assertEquals(pageText3, "Find & reorder past purchases");

        String pageText4 = appiumDriver.findElement(By.id("track_your_packages")).getText();
        Assert.assertEquals(pageText4, "Track your purchases");

    }

    @Test(description = "Check the Amazon sign in page secondarys title")
    public void amazonSigninPageSecondarysTitle() {
        System.out.println("Test amazonSigninPageSecondarysTitle");
        System.out.println("Created mobileDriverDetails --> "+ mobileDriverDetails.getSessionUrl());
        String pageText2 = appiumDriver.findElement(By.id("view_your_wish_list")).getText();
        Assert.assertEquals(pageText2, "View your wish list");

        String pageText3 = appiumDriver.findElement(By.id("Find_purchase")).getText();
        Assert.assertEquals(pageText3, "Find & reorder past purchases");

        String pageText4 = appiumDriver.findElement(By.id("track_your_packages")).getText();
        Assert.assertEquals(pageText4, "Track your purchases");

    }


    @Test(description = "Check sign in the button present on Amazon sign in page")
    public void checkSignInBtn() {
        System.out.println("Test checkSignInBtn");
        System.out.println("Created mobileDriverDetails --> "+ mobileDriverDetails.getSessionUrl());
        WebElement signInBtn = appiumDriver.findElement(By.id("sign_in_button"));
        if (signInBtn.isDisplayed() && signInBtn.isEnabled())
            System.out.println("sign in button is present and its a clickable");
        else {
            System.out.println("sign in button is not present and its not clickable");
        }


    }


    @Test(description = "Check sign up the button present on Amazon sign in page")
    public void checkSignUpBtn() {
        System.out.println("Test checkSignUpBtn");
        System.out.println("Created mobileDriverDetails --> "+ mobileDriverDetails.getSessionUrl());
        WebElement signUpBtn = appiumDriver.findElement(By.id("new_user"));
        if (signUpBtn.isDisplayed() && signUpBtn.isEnabled()) {
            System.out.println("sign Up button is present and its a clickable");
        } else {
            System.out.println("sign Up button is not present and its not clickable");
        }
    }


    @Test(description = "Check skip sign in button on amazon sign in page")
    public void checkSkipSignIpBtn() {
        System.out.println("Test checkSkipSignIpBtn");
        System.out.println("Created mobileDriverDetails --> "+ mobileDriverDetails.getSessionUrl());
        WebElement skipSignInpBtn = appiumDriver.findElement(By.id("skip_sign_in_button"));
        if (skipSignInpBtn.isDisplayed() && skipSignInpBtn.isEnabled()) {
            System.out.println("skip sign in button is present and its a clickable");
        } else {
            System.out.println("skip sign in button is not present and its not clickable");
        }
    }

    @Test(description = "login with Valid UserName and Valid Password")
    public void loginToAmazon() throws InterruptedException {
        System.out.println("Test loginToAmazon");
        System.out.println("Created mobileDriverDetails --> "+ mobileDriverDetails.getSessionUrl());
        appiumDriver.findElement(By.id("sign_in_button")).click();
        String currentview = appiumDriver.getContext();

        if (currentview.equalsIgnoreCase("NATIVE_APP")) {
            WebElement loginUser = appiumDriver.findElement(By.id("ap_email_login"));
            loginUser.sendKeys("akashgupta.gupta16@gmail.com");
            appiumDriver.findElement(By.id("continue")).click();
            try
            {
                WebElement errorMessage = appiumDriver.findElement(By.id("auth-error-message-box"));
                if(errorMessage.isDisplayed())
                {
                    loginUser.clear();
                    appiumDriver.findElement(By.id("ap_email_login")).sendKeys("akashgupta.gupta16@gmail.com");
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("continue")));
                    appiumDriver.findElement(By.id("continue")).click();
                    appiumDriver.findElement(By.id("ap_password")).sendKeys("akash@787");
                    appiumDriver.findElement(By.id("signInSubmit")).click();

                }

            }
            catch (Exception e)
            {
                appiumDriver.findElement(By.id("ap_password")).sendKeys("akash@787");
                appiumDriver.findElement(By.id("signInSubmit")).click();
            }


        } else {
            System.out.println("web view is display");
        }
    }

    @Test(enabled = true, description = "search product without signup")
    public void searchProductWithoutSignIn() throws InterruptedException {
        System.out.println("Test searchProductWithoutSignIn");
        System.out.println("Created mobileDriverDetails --> "+ mobileDriverDetails.getSessionUrl());
        appiumDriver.findElement(By.id("skip_sign_in_button")).click();
        appiumDriver.findElement(By.id("rs_search_src_text")).click();
        Thread.sleep(2000);
        appiumDriver.findElement(By.id("rs_search_src_text")).sendKeys("mobile");
        Thread.sleep(2000);
        appiumDriver.findElement(By.id("iss_search_dropdown_item_text")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("item_title")));
        boolean searchresultpage = appiumDriver.findElement(By.id("item_title")).isDisplayed();
        Assert.assertTrue(searchresultpage);


    }

    @Test(description = "search the product")
    public void checkSearchProduct() throws InterruptedException {
        System.out.println("Test checkSearchProduct");
        loginToAmazon();
        appiumDriver.findElement(By.id("rs_search_src_text")).click();
        Thread.sleep(2000);
        appiumDriver.findElement(By.id("rs_search_src_text")).sendKeys("mobile");
        Thread.sleep(2000);
        appiumDriver.findElement(By.id("iss_search_dropdown_item_text")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("item_title")));
        boolean searchresultpage = appiumDriver.findElement(By.id("item_title")).isDisplayed();
        Assert.assertTrue(searchresultpage);
    }

    @Test
    public void checkAction_bar() throws InterruptedException {
        System.out.println("Test checkAction_bar");
        loginToAmazon();
        System.out.println("Created mobileDriverDetails --> "+ mobileDriverDetails.getSessionUrl());
        appiumDriver.findElement(By.id("action_bar_burger_icon")).click();
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("drawer_item_title")));
    }


}
